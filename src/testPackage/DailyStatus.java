package testPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utility.ExcelToHtml;

import utility.MailReports;

public class DailyStatus {

	public static String sPath, sPathforProperty, sHtmlReportpath,
			sHtmlReportFile;

	@BeforeClass
	public void beforeClass(ITestContext context) {

		sPathforProperty = context.getCurrentXmlTest().getParameter(
				"selenium.mail.properties");

		sPath = context.getCurrentXmlTest().getParameter("sampleReport.path");

	}

	@Test
	public void MainFunction() {
		try {
			String sHTML = new ExcelToHtml(new FileInputStream(new File(sPath)))
					.getHTML();

			MailReports.sendMailOfReport(sPathforProperty, sPath, sHTML);
		} catch (IOException e) {
			e.toString();
		}
	}

	@AfterClass
	public void afterClass() {

		// End of Test Suite//
	}

}
